﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class BossController : MonoBehaviour
    {
        [SerializeField] private BossSpaceship bossSpaceShip;
        [SerializeField] private float enemyShipSpeed = 5;


        
        [SerializeField] float chasingThresholdDistance = 1.0f;
        public bool MoveRight;


        void Update()
        {
            bossSpaceShip.Fire();
            if (MoveRight)
            {
                transform.Translate(2 * Time.deltaTime * enemyShipSpeed, 0, 0);
            }
            else
            {
                transform.Translate(-2 * Time.deltaTime * enemyShipSpeed, 0, 0);
            }

            

        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Turn"))
            {
                if (MoveRight)
                {
                    MoveRight = false;
                }
                else
                {
                    MoveRight = true;
                }
            }
        }



        private bool intersectAABB(Bounds a, Bounds b)
        {
            return (a.min.x <= b.max.x && a.max.x >= b.min.x) && (a.min.y <= b.max.y && a.max.y >= b.min.y);
        }




    }
}