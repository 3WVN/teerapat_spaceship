﻿using System;
using EnemyShip;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private Button startButton;  
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform thank;
        [SerializeField] private RectTransform finalScore;
        [SerializeField] private PlayerSpaceShip playerSpaceship;
        [SerializeField] private EnemySpaceShip enemySpaceship;     
        [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private BossSpaceship bossSpaceship;
        

        

        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipbossHp;
        [SerializeField] private int enemySpaceshipbossMoveSpeed;
        
        public PlayerSpaceShip spawndPlayerSpaceShip;
        
       
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceshipHp hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp hp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            
            
            startButton.onClick.AddListener(OnStartButtonClicked);            
            SoundManager.Instance.PlayBGM();
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
            
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void SpawnPlayerSpaceship()
        {
            spawndPlayerSpaceShip = Instantiate(playerSpaceship);
            spawndPlayerSpaceShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawndPlayerSpaceShip.OnExploded += OnPlayerSpaceshipExploded;
          
        }
        

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
            SceneManager.LoadScene("GameOver");
        }

        private void SpawnEnemySpaceship()
        {
            var spawendEnemyShip = Instantiate(enemySpaceship);
            spawendEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawendEnemyShip.OnExploded += OnEnemySpaceshipExploded;
            
        }
        private void SpawnEnemyBossSpaceship()
        {
            var spawendEnemyShip = Instantiate(bossSpaceship);
            spawendEnemyShip.Init(enemySpaceshipbossHp, enemySpaceshipbossMoveSpeed);
            spawendEnemyShip.OnExploded += OnEnemyBossSpaceshipExploded;

        }

        private void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(50);
            SpawnEnemyBossSpaceship();
           
        }
        private void OnEnemyBossSpaceshipExploded()
        {
            scoreManager.SetScore(200);
            thank.gameObject.SetActive(true);
        }

        private void Restart()
        {
            DestroyRemainingships();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingships()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            var remainingPalyer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPalyer)
            {
                Destroy(playerSpaceship);
            }
        }

        
    }
}
